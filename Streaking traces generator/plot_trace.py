#!/usr/bin/env python3

# April, 2020, ETHZ
# Deep lerning attosecond pulse reconstruction
# Plot streaking traces, IR and XUV pulse


from os import path, mkdir, getcwd
import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import pandas as pd
import math

def plot_trace(folder_name, trace) :
    """ plot trace #trace in folder folder_name """

    h = 6.62606957e-34;
    el = 1.602176565e-19;

    nfig=0
    filename_trace=folder_name +'/trace_' + str(trace) + '.csv'
    filename_labels=folder_name +'/labels_' + str(trace) + '.csv'
    filename_fields=folder_name +'/fields_'+ str(trace) + '.csv'
    filename_spectrum=folder_name +'/spectrum_'+ str(trace) + '.csv'
    filename_tauaxis=folder_name +'/tauaxis' + '.csv'
    filename_Waxis=folder_name +'/Waxis' + '.csv'

    labels=np.loadtxt(filename_labels);
    XUV_center=labels[4]
    XUV_FWHM=labels[3]

    tau=np.loadtxt(filename_tauaxis);
    W=np.loadtxt(filename_Waxis);
    Spec= np.array(pd.read_csv(filename_trace,index_col=None))

    dummy= np.array(pd.read_csv(filename_fields,index_col=None,header=None))
    t= dummy[0,:]
    E_XUV_output= dummy[1,:]
    E_IR_output= dummy[2,:]

    dummy= np.array(pd.read_csv(filename_fields,index_col=None,header=None))
    t= dummy[0,:]
    E_XUV_output= dummy[1,:]
    E_IR_output= dummy[2,:]

    dummy= np.array(pd.read_csv(filename_spectrum,index_col=None,header=None))
    f_xuv= dummy[0,:]
    Exuv_ff_abs= dummy[1,:]
    Exuv_ff_phase= dummy[2,:]

    nfig=nfig+1;
    plt.figure(num=nfig)
    plt.subplot(2,2,1)
    plt.plot(t,E_IR_output/np.max(E_IR_output),color='red')
    plt.plot(t,E_XUV_output/np.max(E_XUV_output),color='blue')
    matplotlib.rc('xtick', labelsize=16) 
    matplotlib.rc('ytick', labelsize=16) 
    plt.xlabel('Time (fs)',fontsize=20)
    plt.ylabel('Norm. Ampl.',fontsize=20)


    #nfig=nfig+1;
    #plt.figure(num=nfig)
    plt.subplot(2,2,2)
    plt.pcolor(tau,W,Spec, cmap='jet')
    plt.ylabel('Electron energy (eV)',fontSize=20)
    plt.xlabel('Delay (fs) (XUV delayed)',fontSize=20)
    matplotlib.rc('xtick', labelsize=16) 
    matplotlib.rc('ytick', labelsize=16)


    #nfig=nfig+1;
    #plt.figure(num=nfig)
    plt.subplot(2,2,3)
    plt.plot(f_xuv,Exuv_ff_abs, color='blue')
    plt.ylabel('XUV spectrum',fontSize=20)
    plt.xlabel('XUV frequency (Hz)',fontSize=20)
    plt.xlim((XUV_center-2*XUV_FWHM)/h*el,(XUV_center+2*XUV_FWHM)/h*el)
    matplotlib.rc('xtick', labelsize=16) 
    matplotlib.rc('ytick', labelsize=16)

    #nfig=nfig+1;
    #plt.figure(num=nfig)
    plt.subplot(2,2,4)
    plt.plot(f_xuv,Exuv_ff_phase, color='blue')
    plt.ylabel('XUV phase',fontSize=20)
    plt.xlabel('XUV frequency (Hz)',fontSize=20)
    plt.ylim(-3*math.pi,3*math.pi)
    plt.xlim((XUV_center-2*XUV_FWHM)/h*el,(XUV_center+2*XUV_FWHM)/h*el)
    matplotlib.rc('xtick', labelsize=16) 
    matplotlib.rc('ytick', labelsize=16) 

    plt.show()

    return

# ========================================================

folder_name='2020_03_30_XUV_all_IR_tau_I'

plot_trace(folder_name, 2)


