#!/usr/bin/env python3

# April, 2020, ETHZ
# Deep lerning attosecond pulse reconstruction
# Generate streaking traces
# Traces parameters can be set

from os import path, mkdir, getcwd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import simu_module

from scipy import constants

import pickle

import time

nfig=0;
plt.close('all')

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#%%% Choose parameters for simulation %%%
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

save_every = 10; # Save a new pickle file every save_every traces
N_traces   = 1;100*save_every;

# Define the output file name
folder_name = time.strftime("%Y_%m_%d", time.localtime())
folder_name += '_XUV_all_IR_tau_I'

plot_single_trace=2; # 1=plots a single trace, 2=produces many data

init_num = 0 # Output pickle files will be numbered as [init_num+1, init_num+2, ...]

# Check the outcommented lines in the loop to select which parameters shall be varied

# IR parameters
IR_tau = 6;""" IR pulse duration (should be half 1/e width) in fs [2,10]"""
IR_tau_bounds=[4,8];
IR_Inten = 2e12; """IR Intensity in W/cm2 [0.5,2]"""
IR_Inten_bounds = [0.5e12,2e12];
IR_cr = 0.; """chirp rate (linear chirp), no specific unit [0,0.1]"""
IR_cr_bounds = [-0.,0.];

# XUV parameters
XUV_center  = 30;   """XUV center in eV"""
XUV_center_bounds=[25,40];
XUV_FWHM    = 20; """XUV FWHM in eV"""
XUV_FWHM_bounds=[5,20];
XUV_p2      = +0.e-2; """%XUV chirp in fs**2 [-0.03,0.03]"""
XUV_p2_bounds = [-0.4e-2,0.4e-2];
XUV_p3      = -0.e-5; """XUV chirp in fs**3 [-0.03,0.03]"""
XUV_p3_bounds = [-4e-4,4e-4];
XUV_p4      = +0.e-5; """XUV chirp in fs**4 [-0.03,0.03]"""
XUV_p4_bounds = [-1e-5,1e-5];
XUV_p5      = +0.e-7; """XUV chirp in fs**5 [-0.03,0.03]"""
XUV_p5_bounds = [-2e-7,2e-7];

# Create a dict with the parameters
sim_param = {
    'E_max' : 40,   # Max kinetic energy in eV
    'E_n'   : 2**6, # points on the energy axis
    'dt'    : 0.1, # step size in fs
    'tau_n' : 80}   # number of delay points (symmetric around overlap)

# Limits for output files
lim_param = {
    't_XUV' : 1.2,               # XUV electric field is saved in the region -t_XUV fs to +t_XUV fs
    'n_XUV' : 256,              # number of pts for output XUV field
    'E_max_XUV_spectrum' : 80., # output spectrum is saved in the region 0eV to E_max_XUV_spectrum eV
    'n_spectrum' : 96}          # number of points for spectrum (intensity and phase)

# Initialize the random generator with the time
np.random.RandomState(seed=None)

if plot_single_trace==1:
    tau, W, Spec, t, E_IR, E_XUV, tau_FWHM, T_IR, Spectrum_XUV = simu_module.simulate_strong_field(
        IR_tau, IR_Inten, IR_cr,
        XUV_FWHM,XUV_center,XUV_p2,XUV_p3,XUV_p4,XUV_p5,
        sim_param,
        lim_param)

    print('IR duration: ', T_IR)
    print('XUV duration:', tau_FWHM)
    print('XUV duration*4:', tau_FWHM*4)

    if (tau_FWHM < T_IR/4.):
    
        plt.figure(100)
        plt.subplot(2,2,1)
        plt.plot(E_IR.t,  E_IR.E  /np.max(E_IR.E),  color='red')
        plt.plot(E_XUV.t, E_XUV.E /np.max(E_XUV.E), color='blue')
        matplotlib.rc('xtick', labelsize=16) 
        matplotlib.rc('ytick', labelsize=16) 
        plt.xlabel('Time (fs)',fontsize=20)
        plt.ylabel('Norm. Ampl.',fontsize=20)

        plt.subplot(2,2,2)
        plt.pcolor(tau, W, Spec, cmap='jet')
        plt.ylabel('Electron energy (eV)',fontSize=20)
        plt.xlabel('Delay (fs) (XUV delayed)',fontSize=20)
        matplotlib.rc('xtick', labelsize=16) 
        matplotlib.rc('ytick', labelsize=16)

        plt.subplot(2,2,3)
        plt.plot(Spectrum_XUV.W,Spectrum_XUV.abs, '-o', color='black')
        plt.ylabel('XUV spectrum',fontSize=20)
        plt.xlabel('XUV frequency (eV)',fontSize=20)
        matplotlib.rc('xtick', labelsize=16) 
        matplotlib.rc('ytick', labelsize=16)

        plt.subplot(2,2,4)
        plt.plot(Spectrum_XUV.W,Spectrum_XUV.phase, '-o', color='orange')
        plt.ylabel('XUV phase',fontSize=20)
        plt.xlabel('XUV frequency (eV)',fontSize=20)
        plt.ylim(-3*constants.pi,3*constants.pi)
        matplotlib.rc('xtick', labelsize=16) 
        matplotlib.rc('ytick', labelsize=16)

        plt.show()

    else :
        print('XUV pulse is too long, change parameters!')
    

elif plot_single_trace==2:
    pwd = path.abspath(getcwd());
    if path.exists(folder_name)==False:
        mkdir(folder_name)
    
    ij=1;

    dpoints = []
    
    while ij <= N_traces:
        print(ij,'/',N_traces);

        # Define the paramters for the IR and the XUV
        IR_tau=IR_tau_bounds[0]+np.random.rand(1)[0]*(IR_tau_bounds[1]-IR_tau_bounds[0]);
        IR_Inten=IR_Inten_bounds[0]+np.random.rand(1)[0]*(IR_Inten_bounds[1]-IR_Inten_bounds[0]);
        IR_cr=IR_cr_bounds[0]+np.random.rand(1)[0]*(IR_cr_bounds[1]-IR_cr_bounds[0]);
        XUV_center=XUV_center_bounds[0]+np.random.rand(1)[0]*(XUV_center_bounds[1]-XUV_center_bounds[0]);
        XUV_FWHM=XUV_FWHM_bounds[0]+np.random.rand(1)[0]*(XUV_FWHM_bounds[1]-XUV_FWHM_bounds[0]);
        XUV_p2=XUV_p2_bounds[0]+np.random.rand(1)[0]*(XUV_p2_bounds[1]-XUV_p2_bounds[0]);
        XUV_p3=XUV_p3_bounds[0]+np.random.rand(1)[0]*(XUV_p3_bounds[1]-XUV_p3_bounds[0]);
        XUV_p4=XUV_p4_bounds[0]+np.random.rand(1)[0]*(XUV_p4_bounds[1]-XUV_p4_bounds[0]);
        XUV_p5=XUV_p5_bounds[0]+np.random.rand(1)[0]*(XUV_p5_bounds[1]-XUV_p5_bounds[0]);

        # Calculate the spectrogram        
        tau, W, Spec, t, E_IR, E_XUV, tau_FWHM, T_IR, Spectrum_XUV = simu_module.simulate_strong_field(
            IR_tau, IR_Inten, IR_cr,
            XUV_FWHM,XUV_center,XUV_p2,XUV_p3,XUV_p4,XUV_p5,
            sim_param,
            lim_param)

        if (tau_FWHM < T_IR/4.):

            labels   = [IR_tau, IR_Inten, IR_cr, XUV_FWHM, XUV_center, XUV_p2, XUV_p3, XUV_p4, XUV_p5, tau_FWHM, T_IR]
            
            dpoints.append({
                'labels'             : labels,
                'spectrogram'        : Spec,
                'E_IR_t'             : E_IR.E,
                'E_XUV_t'            : E_XUV.E,
                'Spectrum_XUV_abs'   : Spectrum_XUV.abs,
                'Spectrum_XUV_phase' : Spectrum_XUV.phase
                })
                
            if ij==1:
                axes = {
                    'tau'  : tau,            # delay axis [fs] in spectrogram
                    'W'    : W,              # energy axis [eV] in spectrogram
                    't_IR' : E_IR.t,         # time axis IR electric field
                    't_XUV' : E_XUV.t,       # time axis XUV electric field
                    'W_XUV' : Spectrum_XUV.W, # energy axis XUV spectrum
                    'labels_header' : 'IR_tau, IR_Inten, IR_cr, XUV_FWHM, XUV_center, XUV_p2, XUV_p3, XUV_p4, XUV_p5, tau_FWHM, T_IR'
                    }

            # --------------- Save output to pickle file ----------------
            if (not (ij % save_every)) :
                f_name = folder_name + '/traces_' + str(init_num + ij//save_every) + '.pickle'
                with open(f_name, 'wb') as fd:
                    pickle.dump((axes, dpoints), fd)

                # Erase saved traces 
                del dpoints
                dpoints = []
                
                print('Traces dumped to pickle file:', f_name)

            ij=ij+1;
    

print('Have fun with the neural nets!')


