#!/usr/bin/env python3

# April, 2020, ETHZ
# Deep lerning attosecond pulse reconstruction
# Calculate the streaking trace

import numpy as np
from math import pi
import matplotlib.pyplot as plt
import matplotlib
from scipy.interpolate import interp1d  
from scipy.optimize import curve_fit

class field:
    """ field class """
    def __init__(self):
        self.t = [] # time axis
        self.E = [] # field axis

    def def_time_axis(self, min_t, max_t, n_points):
        self.t = np.linspace(min_t, max_t, n_points)

class spectrum:
    """ spectrum class """
    def __init__(self):
        self.W     = [] # energy axis
        self.abs   = [] # absolute value of the field
        self.phase = [] # phase of the field
        
    def def_W_axis(self, min_W, max_W, n_points):
        self.W = np.linspace(min_W, max_W, n_points)


# ============================================================

def simulate_strong_field(sigIR, Inten, CR,
                          xuv_FWHM,xuv_c,p2,p3,p4,p5,
                          sim_param,
                          lim_param):
    
#    % everything is in at. un.
#    % p and A are parallel. Formula from Mairesse
    
#    %% constants
    c = 299792458
    h = 6.62606957e-34
    el = 1.602176565e-19
    me = 9.1093826e-31
    eps0 = 8.854e-12
    
#    %au
    t_au = 2.418884326505e-17
    e_au = 4.35974417e-18
    m_au = 5.2917720859e-11
    int_au = 1.9446e-12
    c_au = c/m_au*t_au
    el_au = -1
    
#    %% Parameters which can be varied
#    %IR parameters
    sigIR = sigIR*1e-15/t_au
    CR = CR*1.e30*t_au**2.
    
    p2 = p2*1.e-30
    
#    %IR
    wavelength = 785e-9
    omega_IR = 2*pi*c_au/(wavelength/m_au);
    omega_IR_si = 2*pi*c/(wavelength); """ IR frequency"""
    A0 = np.sqrt(2*Inten*1e4*376.7303)*int_au;  """amplitude of the vector potential (w/cm**2)"""
    Env_ir = lambda t: np.exp(-np.log(4)*t**2/sigIR**2); """gaussian envelope"""
    Eir = lambda t: A0*Env_ir(t)*np.cos(omega_IR*t+CR/2*t**2);
    
    Ip = 15.7596;"""%15.75*el/e_au; %ionization potential of the gas in eV"""

    hh_c = xuv_c/omega_IR_si/h*2*pi*el;
    Nt_xuv = 2**18;
    t_xuv = np.linspace(-80,80,Nt_xuv)*1e-15;
    dt = t_xuv[1]-t_xuv[0];
    df = 1/(Nt_xuv*dt);
    f_xuv = (-Nt_xuv/2+np.arange(0,Nt_xuv+1e-20))*df;
    e_xuv = h*f_xuv/np.abs(el);
    omega_l=c/wavelength*2*pi;
    hnu=h*omega_l/(2*pi*np.abs(el));
    omega_c=2*pi*hh_c*c/wavelength; """%seleziona ordine dispari"""
#    %% Set the spectrum
    c1=2*pi*xuv_FWHM/h*el/2*np.sqrt(np.log(2));
    c1=c1*np.sqrt(2);
#    %% Set the phase  
    Exuv_ff=np.sqrt(pi)*1/c1*(np.exp(-1/c1**2*(2*pi*f_xuv-omega_c)**2)+np.exp(-1/c1**2*(2*pi*f_xuv+omega_c)**2));
    Exuv_ff_abs=Exuv_ff
#    Exuv_ff = Exuv_ff*np.exp(1j*(p2*(f_xuv*2*pi-omega_IR_si*hh_c)**2+\
#                                 p3*((f_xuv*2*pi-omega_IR_si*hh_c)/1e15)**3+\
#                                 p4*((f_xuv*2*pi-omega_IR_si*hh_c)/1e15)**4+\
#                                 p5*((f_xuv*2*pi-omega_IR_si*hh_c)/1e15)**5));  
#    Exuv_ff_phase=p2*(f_xuv*2*pi-omega_IR_si*hh_c)**2+\
#                                 p3*((f_xuv*2*pi-omega_IR_si*hh_c)/1e15)**3+\
#                                 p4*((f_xuv*2*pi-omega_IR_si*hh_c)/1e15)**4+\
#                                 p5*((f_xuv*2*pi-omega_IR_si*hh_c)/1e15)**5; 
                                 
#    Exuv_ff = np.where(f_xuv>0,Exuv_ff*np.exp(1j*((p2*(f_xuv*2*pi-omega_IR_si*hh_c)**2+\
#                                 p3*((f_xuv*2*pi-omega_IR_si*hh_c)/1e15)**3+\
#                                 p4*((f_xuv*2*pi-omega_IR_si*hh_c)/1e15)**4+\
#                                 p5*((f_xuv*2*pi-omega_IR_si*hh_c)/1e15)**5))),
#                                 Exuv_ff);
#    Exuv_ff = np.where(f_xuv<0,Exuv_ff*np.exp(1j*((p2*(f_xuv*2*pi+omega_IR_si*hh_c)**2+\
#                                 p3*((f_xuv*2*pi+omega_IR_si*hh_c)/1e15)**3+\
#                                 p4*((f_xuv*2*pi+omega_IR_si*hh_c)/1e15)**4+\
#                                 p5*((f_xuv*2*pi+omega_IR_si*hh_c)/1e15)**5))),
#                                 Exuv_ff);                               
    Exuv_ff_phase=0*Exuv_ff_abs
    Exuv_ff_phase= np.where(f_xuv>0,(p2*(f_xuv*2*pi-omega_IR_si*hh_c)**2+\
                                 p3*((f_xuv*2*pi-omega_IR_si*hh_c)/1e15)**3+\
                                 p4*((f_xuv*2*pi-omega_IR_si*hh_c)/1e15)**4+\
                                 p5*((f_xuv*2*pi-omega_IR_si*hh_c)/1e15)**5),
                                 Exuv_ff_phase);
    
    Exuv_ff_phase= np.where(f_xuv<0,(p2*(f_xuv*2*pi+omega_IR_si*hh_c)**2+\
                                 -p3*((f_xuv*2*pi+omega_IR_si*hh_c)/1e15)**3+\
                                 p4*((f_xuv*2*pi+omega_IR_si*hh_c)/1e15)**4+\
                                 -p5*((f_xuv*2*pi+omega_IR_si*hh_c)/1e15)**5),
                                 Exuv_ff_phase);
    
    Exuv_ff=Exuv_ff_abs*np.exp(1j*Exuv_ff_phase)                                
    
    
    
#    Exuv_ff_phase=np.unwrap(np.angle(Exuv_ff))
    
    
    
#    %% back transforming
    Exuv_tf = np.fft.ifftshift(np.fft.ifft(np.fft.ifftshift(Exuv_ff)));
    
#    Exuv_tf=Exuv_ti  """ no chirp"""
    Exuv_tf = Exuv_tf/np.max(np.abs(Exuv_tf))
    
#    %% XUV in time 
    E_ab = interp1d(t_xuv ,np.abs(Exuv_tf),'cubic');
    E_ph = interp1d(t_xuv,np.unwrap(np.angle(Exuv_tf))-np.min(np.unwrap(np.angle(Exuv_tf))),'cubic');
    Exuv = lambda t: E_ab(t*t_au)*np.exp(1j*E_ph(t*t_au));
    Exuv_env = lambda t: E_ab(t*t_au);
    
#    %fmax = 2*(hh_c*c/wavelength - 18*el/h)* 1e-15; %max frequency in fs-1
    fmax = sim_param['E_max']*el/h* 1e-15; """%max frequency in fs-1"""
    f = np.linspace(0,fmax,sim_param['E_n']); """%Frequency axis"""
    w = 2*pi*f; """omegas"""
    W = f*1e15*h/el; """energies in eV"""
    
    N_t=2**12;
    t = np.linspace(-30,30,N_t);
    dt = t[2]-t[1];
    
#    %delays %in fs
    step_ratio=np.ceil(sim_param['dt']/dt);
    
    tau = step_ratio*dt*np.linspace(-(sim_param['tau_n']-1)/2,(sim_param['tau_n']-1)/2,sim_param['tau_n']);    
    envelope=Exuv_env(t*1e-15/t_au)/np.max(np.real(Exuv_env(t*1e-15/t_au)));
    
    def gauss(x, A,mu,sigma):
        return A*np.exp(-((x-mu)/sigma)**2)
    
    popt, pcov = curve_fit(gauss,t,envelope,p0=[1,0,0.2]);
    
    tau_FWHM=2*np.sqrt(np.log(2))*popt[2];
    T_IR=2*pi/omega_IR_si*10**15;
    
    if tau_FWHM<T_IR/4.:
        # -------------- Calculate the spectrogram -------------
        A = - np.cumsum(Eir(t*1e-15/t_au))*dt*1e-15/t_au;
        A2 = 0.5*A*A;
    
        # calculating trace with a for loop
        t_xuv_full=np.arange(t[0]+tau[0],t[-1]+tau[-1]+1e-20,dt)
        Exuv_full=Exuv(t_xuv_full*1e-15/t_au);
        
        a_tau = 1j*np.zeros((sim_param['E_n'],sim_param['tau_n']));
    
        for iv in range(0,sim_param['E_n']):    
            Wf = W[iv]*el/e_au; """in a.u."""
            vf = np.sqrt(2*W[iv]*el/e_au); """%final velocity in a.u."""
            phi = -np.flip(np.cumsum(np.flip(vf*A+A2)))*dt*1e-15/t_au; """%gate phase"""
            phi = -np.flip(np.cumsum(np.flip(vf*A+A2)))*dt*1e-15/t_au; """%gate phase"""
            exp_phi_Wf=np.exp(-1j*phi)*np.exp(-1j*(Wf+Ip*el/e_au)*t*1e-15/t_au);
            
            for ij in range(0,sim_param['tau_n']):
                integrand=exp_phi_Wf*Exuv_full[np.arange(step_ratio*(ij),N_t+step_ratio*(ij)).astype(int)];
                a_tau[iv,ij] = -1j*np.sum(integrand);
    
        Spec = np.abs(a_tau*dt*1e-15/t_au)**2;

        # ---------- Generate output field and spectrum for storage ----------------
        # Generate IR and XUV fields for storage
        E_IR_output = field()
        E_IR_output.def_time_axis(tau[0], tau[-1], sim_param['tau_n'])
        E_IR_output.E = Eir(E_IR_output.t*1e-15/t_au)

        E_XUV_output = field()
        E_XUV_output.def_time_axis(-1.*lim_param['t_XUV'], lim_param['t_XUV'], lim_param['n_XUV'])
        E_XUV_output.E = np.real(Exuv(E_XUV_output.t*1e-15/t_au))
        
        # Generate XUV spectrum
        Spectrum_XUV_output = spectrum()
        Spectrum_XUV_output.def_W_axis(0., lim_param['E_max_XUV_spectrum'], lim_param['n_spectrum'])
        Exuv_W_abs_interp   = interp1d(f_xuv*h/el, Exuv_ff_abs)   # Abs value of the XUV electric field vs eV
        Exuv_W_phase_interp = interp1d(f_xuv*h/el, Exuv_ff_phase) # Phase of the XUV electric field vs eV
        Spectrum_XUV_output.abs   = Exuv_W_abs_interp  (Spectrum_XUV_output.W)
        Spectrum_XUV_output.phase = Exuv_W_phase_interp(Spectrum_XUV_output.W)
        
    else:
        W=None
        Spec=None
        E_IR_output=None
        E_XUV_output=None
        Spectrum_XUV_output=None

    return tau, W, Spec, t, E_IR_output, E_XUV_output, tau_FWHM, T_IR, Spectrum_XUV_output
    
