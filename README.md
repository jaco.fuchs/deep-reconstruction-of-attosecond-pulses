# Extreme ultraviolet pulse reconstruction

Study and development of two neural network architectures for extreme ultraviolet pulse reconstruction

**Usage:**
1.  Generate the traces
    1a. Directly use the python script *Streaking traces generator*. At the moment it still needs some debugging (14.5.2020)
    1b. Use the matlab script *Streaking traces generator_matlab* and convert the output to pickle with the script *matlab2python.ipynb*
    1c. Download some pre-generated traces (ideally the 120k traces)
2. Train the p2s and the s2p networks (or use pre-trained weights)
3. Load the trained weights in the selfnet
4. Try it on experimental traces


**Folder structure:**
*  NNs_weights: contains pre-trained weights for the neural nets
*  p2s: densely connected network to go from pulse (IR, XUV spectral intensity and phase) to spectrogram
*  s2p: resnet to go from spectrogram to pulse
*  selfnet: combines the s2p and the p2s networks and allows to retrain the s2p netwrok to find self consistent solutions
*  Streaking traces generator: python streaking traces generator
*  Streaking traces generator_matlab: matlab streaking trace generator. The latest version is *getspectra004.m* which also calculated the spectral intensity and phase. The generated traces are stores in mat files
*  load_traces.ipynb: loads pickle files and display the traces (just a playground to look at the data)
*  matlab2python.ipynb: convert the mat files in pickle files that can be given as input for all the other scripts
*  Dataset description.xlsx: description of the datasets generated for training/testing


**Same generated streaking traces:**
* *2020_04_14_XUV_all_IR_tau_I_100k*: 100k traces generated with the python straking trace generator [link](https://www.dropbox.com/sh/5k7lmwdu0plrzdk/AAAzJNCwpk5iMDw3Yq0RREtra?dl=0)
* *2020_05_13_XUV_all_IR_tau_I_120k*: 120k traces generated with the matlab script and pickled in python pickles files [link](https://www.dropbox.com/sh/hh9wovyw111grhr/AADuTGYFqIzSifY_mjpObirya?dl=0)

This project originated from the final task of the Deep learning course, ETH Zurich
J. Fuchs, N. Hartmann, L. Pedrelli, F. Saltarelli
Dec 2019 - Jan 2020