% Calculate the FFT of the input electric field

%%
lambda = 785e-9;
f_cent = 299792458/lambda;

t_au = 2.418884326505e-17;

disp(['Central freq [Hz]:', num2str(f_cent)])
disp(['Central freq [THz]:', num2str(f_cent/1e12)])

%% Prepare time axis
t_fs=t*1e-15;
dt=t_fs(2)-t_fs(1);
L=length(t_fs);

Fs=1/dt;
f = Fs*(0:(L/2))/L;
l_nm = 299792458./f*1e9;

%sigIR = sigIR*1e-15/t_au;

%% ========================================================
%% ======================= IR =============================
%% ========================================================

%% Transform IR field
E_ir = E_IR_output/max(E_IR_output);

E_ir = fftshift(E_ir); % To get rid of time delay in FFTs

figure(200); clf
subplot(3,1,1)
plot(t_fs, E_ir)

E_ir_fft = fft(E_ir);
E_ir_fft_singleside=E_ir_fft(1:L/2+1);
E_ir_fft_singleside(2:end-1)=2*E_ir_fft_singleside(2:end-1);

E_ir_fft_singleside( abs(E_ir_fft_singleside)<0.01*max(abs(E_ir_fft_singleside)) ) = nan;


%% Plot IR field
subplot(3,1,2)
plot(f, abs(E_ir_fft_singleside).^2, '-o')
%plot(l_nm, abs(E_ir_fft_singleside).^2, '-o')
ylabel('E field')

yyaxis right
plot(f, unwrap(angle(E_ir_fft_singleside)),'-o')
%plot(l_nm, unwrap(angle(E_ir_fft_singleside)),'-o')
ylabel('E phase')

%% Plot IR field (and PHASE DERIVATIVE)
subplot(3,1,3)
plot(f, abs(E_ir_fft_singleside).^2, '-o')
%plot(l_nm, abs(E_ir_fft_singleside).^2, '-o')
ylabel('E field')

yyaxis right
plot(f(1:end-1), diff(unwrap(angle(E_ir_fft_singleside))),'-o')
%plot(l_nm, unwrap(angle(E_ir_fft_singleside)),'-o')
ylabel('E phase')

%% ========================================================
%% ======================= XUV ============================
%% ========================================================


% Prepare XUV field
E_XUV = E_XUV_output/max(E_XUV_output);

E_XUV = fftshift(E_XUV); % To get rid of time delay in FFTs

figure(301); clf;
subplot(3,1,1)
plot(t_fs, E_XUV)

E_XUV_fft = fft(E_XUV);
E_XUV_fft_singleside=E_XUV_fft(1:L/2+1);
E_XUV_fft_singleside(2:end-1)=2*E_XUV_fft_singleside(2:end-1);

% Look for the indexes where the intensity of the field is not large enough
% for a meaningful phase calculation
small_values_idx = abs(E_XUV_fft_singleside)<0.45*max(abs(E_XUV_fft_singleside));

E_XUV_fft_singleside( small_values_idx ) = nan;

%% Plot XUV field
subplot(3,1,2)
plot(f, abs(E_XUV_fft_singleside).^2, '-o')
%plot(l_nm, abs(E_XUV_fft_singleside).^2, '-o')

yyaxis right
plot(f, unwrap(angle(E_XUV_fft_singleside)),'-o')
%plot(l_nm, unwrap(angle(E_XUV_fft_singleside)),'-o')

%xlim([20,70])

%% Plot XUV field (with PHASE DERIVATIVE)
subplot(3,1,3)
plot(f, abs(E_XUV_fft_singleside).^2, '-o')
%plot(l_nm, abs(E_XUV_fft_singleside).^2, '-o')

yyaxis right

% Derivative of the fft
d_fft = diff(unwrap(angle(E_XUV_fft_singleside)));

% Search for the max of the XUV field intensity
[~, argmax] = max(abs(E_XUV_fft_singleside).^2);

% Set the phase at the center of the pulse to 0
% d_fft = d_fft - d_fft(argmax);
%d_fft = d_fft - 1./3. * ( d_fft(argmax-1)+d_fft(argmax)+d_fft(argmax+1) );
% Calculate a weighted average to remove the linear slope
weights = 0.375*[1/3, 1/2, 1, 1/2, 1/3];
d_fft = d_fft - 1. * weights * d_fft(argmax-2:argmax+2);
d_fft(isnan(d_fft))=0.; % Convert nans to 0

% Integrate the phase derivative to get back the phase
i_fft = cumtrapz(d_fft);

% Set back to nan arrays location where we do not have phase information
i_fft( small_values_idx(2:end) ) = nan;

% Asign phase=0 to the center of the pulse
i_fft = i_fft - i_fft(argmax);

plot(f(1:end-1), i_fft,'-o')
%plot(l_nm, unwrap(angle(E_XUV_fft_singleside)),'-o')

%xlim([20,70])

% subplot(1,2,2)
% plot(f, unwrap(angle(E_XUV_fft_singleside)),'-o')


