%%
phT=angle(test_s);
abT=abs(test_s);
low_int = abT<0.01;
abT(low_int)=NaN; % Such that itensity and phase are plotted with the same xlim
phT(low_int)=NaN;
phT=unwrap(phT);
phT=phT-min(phT);

%%
figure(777); clf;
hold on
plot(t,abT,'-k')
set(gcf,'Color',[1 1 1])
set(gca,'FontSize',16)
xlabel('Time (fs)','FontSize',18)
ylabel('Norm. Int','FontSize',18)
box on

figure(778); clf;
hold on
plot(t,unwrap(phT),'-k')
set(gcf,'Color',[1 1 1])
set(gca,'FontSize',16)
xlabel('Time (fs)','FontSize',18)
ylabel('Phase','FontSize',18)
box on

%%
% Look for the center in time of abT
t_center = nansum(t'.*abT.*abT) / nansum(abT.*abT);
t_center

%%
L=length(test_s);
Fs=1/(dt);
f = Fs*(0:(L/2))/L;
f_ev = f./2.417989242e14;

E_XUV_fft = fft(test_s);
E_XUV_fft = E_XUV_fft(1:L/2+1);

ab_E_XUV_FFT = abs(E_XUV_fft);
ph_E_XUV_FFT = angle(E_XUV_fft);
%save('temp.mat', 'ph_E_XUV_FFT')
low_int = ab_E_XUV_FFT<1;
ab_E_XUV_FFT(low_int)=NaN;
ph_E_XUV_FFT(low_int)=NaN;

%E_XUV_fft_singleside(2:end-1)=2*E_XUV_fft_singleside(2:end-1);

% Plot XUV field
figure(779); clf;
plot(f_ev, ab_E_XUV_FFT.^2, 'k-')

figure(780); clf;
hold on;
plot(f_ev, unwrap(ph_E_XUV_FFT),'k-')
plot(f_ev, unwrap(ph_E_XUV_FFT)-t_center*1e-15*2.*pi*(f'-40*2.417989242e14),'r-')
%plot(f_ev, unwrap(ph_E_XUV_FFT)-pi*linspace(0,length(ph_E_XUV_FFT),length(ph_E_XUV_FFT))','k-')


