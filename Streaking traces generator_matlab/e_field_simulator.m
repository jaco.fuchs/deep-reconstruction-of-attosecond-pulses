
t_au = 2.418884326505e-17;

sigIR = 9.; %[fs] %IR_tau = 9;% IR pulse duration (should be half 1/e width) in fs [2,10]
sigIR = sigIR*1e-15/t_au; % Conversion to au

Inten = 2e12; %IR_Inten = 2e12; %IR Intensity in W/cm2 [0.5,2]
CR = 0.;4e-5; %IR_cr = 0.; %chirp rate (linear chirp), no specific unit [0,0.1] A(omega)= sin[omega*t + IR_cr*t^2]

%
N_t=2^12;
%t = linspace(-30,30,N_t);
t = linspace(-50,50,N_t);
dt = t(2)-t(1);

%% constants
c = 299792458;
h = 6.62606957e-34;
el = 1.602176565e-19;
me = 9.1093826e-31;
eps0 = 8.854e-12;

%au
t_au = 2.418884326505e-17;
e_au = 4.35974417e-18;
m_au = 5.2917720859e-11;
int_au = 1.9446e-12;
c_au = c/m_au*t_au;
el_au = -1;

%% field definition

%IR
lambda = 785e-9;
%sigIR = 6e-15/t_au; % IR pulse time duration
%Inten = 2e12; %in W/cm2
omega_IR = 2*pi*c_au/(lambda/m_au); % IR frequency
omega_IR_si = 2*pi*c/(lambda); % IR frequency
A0 = sqrt(2*Inten*1e4*376.7303)*int_au;  %amplitude of the vector potential (w/cm^2)
%CR = 0.1e30*t_au^2; %cirp rate 0.1
Env_ir = @(t) exp(-log(4)*t.^2/sigIR^2); %gaussian envelope

Eir = @(t) A0*Env_ir(t).*cos(omega_IR*t+CR/2*t.^2);
%Eir = @(t) A0*Env_ir(t)%.*cos(omega_IR*t+CR/2*t.^2);
%Eir = @(t) cos(omega_IR*t+CR/2*t.^2);

E_IR_output=Eir(t*1e-15/t_au);

compute_fft