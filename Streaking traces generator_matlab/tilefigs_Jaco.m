function tilefigs(figs)
%TILEFIGS Tile all open figure windows around on the screen.
% TILEFIGS places all open figure windows around on the screen with no
% overlap. TILEFIGS(FIGS) can be used to specify which figures that
% should be tiled. Figures are not sorted when specified.
% 2017-02-23 08:48:20 Jaco Fuchs <jafuchs@phys.ethz.ch>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get the handles to the figures to process.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if ( nargin == 0 ) % If no input arguments...
    figsunsrt = findobj( 'Type', 'figure' ); % ...find all figures.
end

if isempty( figsunsrt )
    disp( 'No open figures or no figures specified.' );
    return;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get the screen paramters.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

set( 0, 'Units', 'pixels' ); % Set root units.
scrdim = get(0, 'MonitorPositions'); % Get screen size.
scrdim=sortrows(scrdim); %Begin from the most left screen.
nscr   = length(scrdim(:,1)); %Get number of monitors.

scrwid=scrdim(:,3);%Get the widths
scrhgt=scrdim(:,4);%Get the heights
horoffset=scrdim(:,1)-1;%Get horizontal offset of the screens
veroffset=scrdim(:,2)-1;%Get horizontal offset of the screens
scrsizes=scrdim(:,3).*scrdim(:,4); %Get the screen sizes

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get Figures and sort them.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figs=figsunsrt;
nfigs = length(figs); % Number of figures.
for i=1:nfigs
    figs(get(figsunsrt(i),'Number'))=figsunsrt(i);
end

nfigsscr=zeros(1,nscr);

for s=1:nscr
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Distribute figures to screens.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    nfigsscr(s)=ceil((nfigs-sum(nfigsscr)).*scrsizes(s)./sum(scrsizes(s:end)));

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Set miscellaneous parameter.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    nh = ceil(sqrt(nfigsscr(s))); % Number of figures horisontally.
    nv = ceil( nfigsscr(s)/nh ); % Number of figures vertically.
    
    nh = max( nh, 2 );
    nv = max( nv, 2 );
    
    if s<nscr    
        nfigsscr(s)=nh*nv;
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % The elements in the vector specifying the position.
    % 1 - Window left position
    % 2 - Window bottom position
    % 3 - Window horizontal size
    % 4 - Window vertical size
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    hspc = 50; % Horisontal space.
    topspc = 80; % Space above top figure.
    medspc = 100; % Space between figures.
    botspc = 40; % Space below bottom figure.
    
    figwid = ( scrwid(s) - (nh+1)*hspc )/nh;
    fighgt = ( scrhgt(s) - (topspc+botspc) - (nv-1)*medspc )/nv;
    
    figwid = round(figwid);
    fighgt = round(fighgt);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Put the figures where they belong.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    for row = 1:nv
        for col = 1:nh
            idx = (row-1)*nh + col;
            if ( idx <= nfigsscr(s) )
                figlft = col*hspc + (col-1)*figwid;
                figbot = scrhgt(s) - topspc - row*fighgt - (row-1)*medspc;
                figpos = [ figlft+horoffset(s) figbot+veroffset(s) figwid fighgt ]; % Figure position.
                fighnd = figs(idx+sum(nfigsscr(1:s))-nfigsscr(s)); % Figure handle.
                units = get( fighnd, 'Units' ); % Get units.
                set( fighnd, 'Units', 'pixels' ); % Set new units.
                set( fighnd, 'Position', figpos ); % Set position.
                figure( fighnd ); % Raise figure.
            end
        end
    end
end
end