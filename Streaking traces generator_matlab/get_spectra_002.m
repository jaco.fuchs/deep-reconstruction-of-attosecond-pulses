close all
clearvars
nfig=0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Choose parameters for simulation %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
N_traces=5;
folder_name='example_folder_001';
plot_single_trace=2; %1=plots a single trace, 2=produces many data

%Check the outcommented lines in the loop to select which parameters shall
%be varied


%IR parameters
IR_tau = 6;% IR pulse duration (should be half 1/e width) in fs [2,10]
IR_tau_bounds=[2,10];
IR_Inten = 2e12; %IR Intensity in W/cm2 [0.5,2]
IR_Inten_bounds = [0.5e12,2e12];
IR_cr = 0.0; %chirp rate (linear chirp), no specific unit [0,0.1]
IR_cr_bounds = [-0.10,0.10];

%XUV parameters
XUV_center=35;   %XUV center in eV
XUV_center_bounds=[25,45];
XUV_FWHM=20; %XUV FWHM in eV
XUV_FWHM_bounds=[5,25];
XUV_p2 = 0e-2; %XUV chirp in fs^2 [-0.03,0.03]
XUV_p2_bounds = [-3e-2,3e-2];
XUV_p3 = 0e-3; %XUV chirp in fs^3 [-0.03,0.03]
XUV_p3_bounds = [-4e-4,4e-4];
XUV_p4 = -0e-4; %XUV chirp in fs^4 [-0.03,0.03]
XUV_p4_bounds = [-4e-5,4e-5];
XUV_p5 = -0e-5; %XUV chirp in fs^5 [-0.03,0.03]
XUV_p5_bounds = [-4e-6,4e-6];

%energy axis axis
E_max=40;%Max kinetic energy in eV
E_n = 2^6;%points on the energy axis

%time axis
step_size = 0.25; %step size in fs
N_steps = 80; %number of delay points (symmetric around overlap)



if plot_single_trace==1
    [tau,W,Spec,t,E_IR_output,E_XUV_output,tau_FWHM,T_IR]=function_Streaking_strong_field_Jaco(IR_tau,IR_Inten,IR_cr,XUV_FWHM,XUV_center,XUV_p2,XUV_p3,XUV_p4,XUV_p5,E_max,E_n,step_size,N_steps);
    
    nfig=nfig+1;
    figure(nfig)
    clf
    hold on
    plot(t,E_IR_output/max(E_IR_output),'Color','r')
    plot(t,E_XUV_output/max(E_XUV_output),'Color','b')
    hold on
    set(gcf,'Color',[1 1 1])
    set(gca,'FontSize',16)
    xlabel('Time (fs)','FontSize',18)
    ylabel('Norm. Ampl.','FontSize',18)
    box on
    
    nfig=nfig+1;
    figure(nfig)
    pcolor(tau,W,Spec)
    ylabel('Electron energy (eV)','FontSize',20)
    xlabel('Delay (fs) (XUV delayed)','FontSize',20)
    set(gcf,'Color',[1 1 1])
    set(gca,'FontSize',18)
    shading interp
    colorbar
    
end

if plot_single_trace==2
    path=pwd;
    if 7~=exist(folder_name,'dir')
        mkdir(folder_name)
    end
    
    ij=1;
    while ij<=N_traces
        disp([num2str(ij),'/',num2str(N_traces)]);
        
%         IR_tau=IR_tau_bounds(1)+rand*(IR_tau_bounds(2)-IR_tau_bounds(1));
%         IR_Inten=IR_Inten_bounds(1)+rand*(IR_Inten_bounds(2)-IR_Inten_bounds(1));
%         IR_cr=IR_cr_bounds(1)+rand*(IR_cr_bounds(2)-IR_cr_bounds(1));
        XUV_center=XUV_center_bounds(1)+rand*(XUV_center_bounds(2)-XUV_center_bounds(1));
        XUV_FWHM=XUV_FWHM_bounds(1)+rand*(XUV_FWHM_bounds(2)-XUV_FWHM_bounds(1));
        XUV_p2=XUV_p2_bounds(1)+rand*(XUV_p2_bounds(2)-XUV_p2_bounds(1));
%         XUV_p3=XUV_p3_bounds(1)+rand*(XUV_p3_bounds(2)-XUV_p3_bounds(1));
%         XUV_p4=XUV_p4_bounds(1)+rand*(XUV_p4_bounds(2)-XUV_p4_bounds(1));
%         XUV_p5=XUV_p5_bounds(1)+rand*(XUV_p5_bounds(2)-XUV_p5_bounds(1));
        
        
        [tau,W,Spec,t,E_IR_output,E_XUV_output,tau_FWHM,T_IR]=function_Streaking_strong_field_Jaco(IR_tau,IR_Inten,IR_cr,XUV_FWHM,XUV_center,XUV_p2,XUV_p3,XUV_p4,XUV_p5,E_max,E_n,step_size,N_steps);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%     return values      %%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %tau in fs,
        %W in eV
        %t in fs
        %tau_FWHM in fs
        %T_IR in fs
        
        if tau_FWHM<T_IR/2
            
            labels=[IR_tau,IR_Inten,IR_cr,XUV_FWHM,XUV_center,XUV_p2,XUV_p3,XUV_p4,XUV_p5,tau_FWHM,T_IR];
            fields=[t',E_XUV_output,E_IR_output'];
            
            filename_trace=[folder_name,'/trace_',num2str(ij,'%05.f'),'.csv'];
            filename_labels=[folder_name,'/labels_',num2str(ij,'%05.f'),'.csv'];
            filename_fields=[folder_name,'/fields_',num2str(ij,'%05.f'),'.csv'];
            
            writematrix(Spec,filename_trace);
            writematrix(labels,filename_labels);
            writematrix(fields,filename_fields);
            ij=ij+1;
        end
        
            nfig=nfig+1;
            figure(nfig)
            pcolor(tau,W,Spec)
            ylabel('Electron energy (eV)','FontSize',20)
            xlabel('Delay (fs) (XUV delayed)','FontSize',20)
            set(gcf,'Color',[1 1 1])
            set(gca,'FontSize',18)
            shading interp
            colorbar
        
    end
    
end

tilefigs_Jaco