% close all
clearvars
nfig=0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Choose parameters for simulation %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
N_traces = 40000;
folder_name='c:\Users\FoS\Desktop\2020_05_13_XUV_all_IR_tau_I_10k'; % folder where data are generated (if plot_single_trace=2)
plot_single_trace=1; %1=plots a single trace (neglect N_traces), 2=produces many data
init_num = 1500; % Traces will be numbered as [init_num+1, init_num+2, ..., init_num+N_traces-1]
save_every = 5000; % Save a new file every save_every traces


%Check the outcommented lines in the loop to select which parameters shall
%be varied

% Limits for output files
lim_param = struct(...
    't_XUV', 1.2,...              % XUV electric field is saved in the region -t_XUV fs to +t_XUV fs
    'n_XUV', 256,...              % number of pts for output XUV field
    'E_max_XUV_spectrum', 80.,... % output spectrum is saved in the region 0eV to E_max_XUV_spectrum eV
    'n_spectrum', 96);          % number of points for spectrum (intensity and phase)


%IR parameters
IR_tau = 6;% IR pulse duration (should be half 1/e width) in fs [2,10]
IR_tau_bounds=[4,9]; % Boundaries used to generate multiple traces
IR_Inten = 2e12; %IR Intensity in W/cm2 [0.5,2]
IR_Inten_bounds = [0.5e12,2e12];
IR_cr = 0.; %chirp rate (linear chirp), no specific unit [0,0.1] A(omega)= sin[omega*t + IR_cr*t^2]
IR_cr_bounds = [-0.,0.];

%XUV parameters (all is generated in Argon)
XUV_center = 30;   %XUV center in eV
XUV_center_bounds=[25,45];
XUV_FWHM   = 15; %XUV FWHM in eV
XUV_FWHM_bounds=[5,25];
XUV_p2 = +0.6e-2; %XUV chirp in fs^2 [-0.03,0.03]
XUV_p2_bounds = [-2e-2,2e-2];
XUV_p3 = +2.5e-4; %XUV chirp in fs^3 [-0.03,0.03]
XUV_p3_bounds = [-4e-4,4e-4];
XUV_p4 = -0.8e-5; %XUV chirp in fs^4 [-0.03,0.03]
XUV_p4_bounds = [-1e-5,1e-5];
XUV_p5 = -0.6e-7; %XUV chirp in fs^5 [-0.03,0.03]
XUV_p5_bounds = [-2e-7,2e-7];

%energy axis
E_max=40;%Max kinetic energy in eV
E_n = 2^6; %2^6;%points on the energy axis

%time axis
step_size = 0.25; %step size in fs
N_steps   = 80; %80; %number of delay points (symmetric around overlap)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Compute the streaking trace(s)   %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Compute time axis for output files
t_XUV_output = linspace(-1.*lim_param.t_XUV, 1.*lim_param.t_XUV, lim_param.n_XUV);
E_axis_XUV_output = linspace(0., lim_param.E_max_XUV_spectrum, lim_param.n_spectrum);

if plot_single_trace==1
    [tau,W,Spec,t,E_IR_output,E_XUV_output,tau_FWHM,T_IR]=function_Streaking_strong_field_Jaco(IR_tau,IR_Inten,IR_cr,XUV_FWHM,XUV_center,XUV_p2,XUV_p3,XUV_p4,XUV_p5,E_max,E_n,step_size,N_steps);
    
    disp(['XUV duration:', num2str(tau_FWHM)])
    disp(['IR duration: ', num2str(T_IR)])
    
    %compute_fft to obtain XUV power spectrum and spectral phase
    [Spec_XUV_abs2, Spec_XUV_phase, f_eV] = compute_fft(t, E_IR_output, E_XUV_output, 0);
    
    if (tau_FWHM>T_IR/2)
        disp('INVALID TRACE!')
    end

    nfig=nfig+1;
    figure(nfig); clf ;
    subplot(3,1,1)
    hold on
    plot(t,E_IR_output/max(E_IR_output),'Color','r')
    plot(t,E_XUV_output/max(E_XUV_output),'Color','b')
    hold on
    set(gcf,'Color',[1 1 1])
    xlabel('Time (fs)')
    ylabel('Norm. Ampl.')
    box on
    
    subplot(3,1,2)
    pcolor(tau,W,Spec)
    ylabel('Electron energy (eV)')
    xlabel('Delay (fs) (XUV delayed)')
    set(gcf,'Color',[1 1 1])
    shading interp
    colorbar
    
    subplot(3,1,3)
    plot(f_eV, Spec_XUV_abs2, '-o')
    xlabel('Electron energy (eV)')
    ylabel('Power spectrum')
    yyaxis right
    plot(f_eV, Spec_XUV_phase,'-o')
    ylabel('Spectral phase [rad]')
        
end

if plot_single_trace==2
    path=pwd;
    if 7~=exist(folder_name,'dir')
        mkdir(folder_name)
    end

    % Random seed
    rng('shuffle')
    
    % Save the time and energy axis of the FROG-CRAB trace (geneate one
    % FROG trace only to get the axes)
    [tau,W,Spec,t,E_IR_output,E_XUV_output,tau_FWHM,T_IR]=function_Streaking_strong_field_Jaco(IR_tau,IR_Inten,IR_cr,XUV_FWHM,XUV_center,XUV_p2,XUV_p3,XUV_p4,XUV_p5,E_max,E_n,step_size,N_steps);
    %writematrix(tau, [folder_name,'/tau_axis','.csv'])
    %writematrix(W  , [folder_name,'/W_axis'  ,'.csv'])
    %disp('tau and W axis saved to csv files')
    
    t_IR_output = tau; % Output IR field will be saved on the same grid used for the spectrogram
    labels_header = ['IR_tau, IR_Inten, IR_cr, XUV_FWHM, XUV_center, XUV_p2, XUV_p3, XUV_p4, XUV_p5, tau_FWHM, T_IR'];
    
    % Save axes to file
    f_name = [folder_name, '/traces_','axes.mat'];
    save(f_name, 'tau', 'W', 't_IR_output','t_XUV_output', 'E_axis_XUV_output', 'labels_header')
    spec = zeros([save_every,size(Spec)]);
    disp(['axes file saved to file ', f_name])
    
    % Prepare output variables
    Spec_array           = zeros([save_every, size(Spec)]);
    labels_array         = zeros([save_every, 11]); % Hard coded #labels saved
    E_IR_t_array         = zeros([save_every, length(t_IR_output)]);
    E_XUV_t_array        = zeros([save_every, length(t_XUV_output)]);
    spec_XUV_abs_array   = zeros([save_every, length(E_axis_XUV_output)]);
    spec_XUV_phase_array = zeros([save_every, length(E_axis_XUV_output)]);  
    
    % Downsampled axes for output file
    E_XUV_fit = fit(t.',E_XUV_output,'cubicspline');
    E_IR_fit  = fit(t.',E_IR_output', 'cubicspline');
    
    ij=1;
    ij_tot=1;
    while ij_tot<=N_traces
        disp([num2str(ij_tot),'/',num2str(N_traces)]);
        
        % Define the parameters of the IR and XUV pulse. They are uniformly
        % distributed in the defined range
        IR_tau=IR_tau_bounds(1)+rand*(IR_tau_bounds(2)-IR_tau_bounds(1));
        IR_Inten=IR_Inten_bounds(1)+rand*(IR_Inten_bounds(2)-IR_Inten_bounds(1));
        IR_cr=IR_cr_bounds(1)+rand*(IR_cr_bounds(2)-IR_cr_bounds(1));
        XUV_center=XUV_center_bounds(1)+rand*(XUV_center_bounds(2)-XUV_center_bounds(1));
        XUV_FWHM=XUV_FWHM_bounds(1)+rand*(XUV_FWHM_bounds(2)-XUV_FWHM_bounds(1));
        XUV_p2=XUV_p2_bounds(1)+rand*(XUV_p2_bounds(2)-XUV_p2_bounds(1));
        XUV_p3=XUV_p3_bounds(1)+rand*(XUV_p3_bounds(2)-XUV_p3_bounds(1));
        XUV_p4=XUV_p4_bounds(1)+rand*(XUV_p4_bounds(2)-XUV_p4_bounds(1));
        XUV_p5=XUV_p5_bounds(1)+rand*(XUV_p5_bounds(2)-XUV_p5_bounds(1));
        
        [tau,W,Spec,t,E_IR_output,E_XUV_output,tau_FWHM,T_IR]=...
            function_Streaking_strong_field_Jaco(IR_tau,IR_Inten,IR_cr,XUV_FWHM,XUV_center,XUV_p2,XUV_p3,XUV_p4,XUV_p5,E_max,E_n,step_size,N_steps);
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%     return values      %%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %tau in fs (straking x axis),
        %W in eV (streaking y axis),
        %spec (@2D histogram, straking trace),
        %t in fs (time axis for E field IR and XUV),
        %E_IR_output, E_XUV_output (E field of IR and XUV, NOT the vector potential),
        %tau_FWHM in fs (gaussian fit to the XUV pulse giving the FWHM pulse duration of the XUV),
        %T_IR in fs (cycle time of the IR, fixed given lambda)
        
        % Check if the FWHM of the XUV is shorter than half a cycle of the IR
        if tau_FWHM<T_IR/4.
           
            %compute_fft to obtain XUV power spectrum and spectral phase
            [Spec_XUV_abs2, Spec_XUV_phase, f_eV] = compute_fft(t, E_IR_output, E_XUV_output, 0);
            
            % Replace nans with 0 in the XUV spectrum power and phase (nans are
            % assigned to low intensity)
            Spec_XUV_abs2(isnan(Spec_XUV_abs2)) = 0.;
            Spec_XUV_phase(isnan(Spec_XUV_phase)) = 0.;
            first_nan = find(~isnan(Spec_XUV_phase),1,'first');
            last_nan  = find(~isnan(Spec_XUV_phase),1,'last');
            Spec_XUV_phase(1:first_nan) = Spec_XUV_abs2(first_nan);
            Spec_XUV_phase(last_nan:end) = Spec_XUV_abs2(last_nan);

            Spectrum_XUV_abs_fit   = fit(f_eV',Spec_XUV_abs2,'cubicspline');
            Spectrum_XUV_phase_fit = fit(f_eV',Spec_XUV_phase,'cubicspline');

            E_XUV_t = E_XUV_fit(t_XUV_output);
            E_IR_t  = E_IR_fit(tau);
            Spectrum_XUV_abs = Spectrum_XUV_abs_fit(E_axis_XUV_output);
            Spectrum_XUV_phase = Spectrum_XUV_phase_fit(E_axis_XUV_output);

            % Plot only for debuggind
%             figure(666); clf;
%             subplot(1,2,1)
%             plot(t_XUV_output, E_XUV_t, 'b',...
%                 tau, E_IR_t, 'r') 
%             subplot(1,2,2)
%             plot(E_axis_XUV_output, Spectrum_XUV_abs, 'b')
%             yyaxis right
%             hold on
%             plot(E_axis_XUV_output, Spectrum_XUV_phase, 'r')
%             %plot(E_axis_XUV_output, Spectrum_XUV_phase.*Spectrum_XUV_abs, 'b--')
            
            % Write to file
            % Labels is what is written in the output file
            % Fields are written as 3 columns in the file
            labels = [IR_tau,IR_Inten,IR_cr,XUV_FWHM,XUV_center,XUV_p2,XUV_p3,XUV_p4,XUV_p5,tau_FWHM,T_IR];
            
            trace_rel_id = mod(ij,save_every);
            
            if (~trace_rel_id)
                % This is the last trace in this batch
                trace_rel_id = save_every;
            end
            Spec_array(trace_rel_id,:,:) = Spec;
            labels_array(trace_rel_id,:) = labels;
            E_IR_t_array(trace_rel_id,:) = E_IR_t;
            E_XUV_t_array(trace_rel_id,:) = E_XUV_t;
            spec_XUV_abs_array(trace_rel_id,:) = Spectrum_XUV_abs;
            spec_XUV_phase_array(trace_rel_id,:) = Spectrum_XUV_phase;
            
            %fields = [t',E_XUV_output,E_IR_output'];
            
            %filename_trace =[folder_name,'/trace_', num2str(ij+init_num,'%07.f'),'.csv'];
            %filename_labels=[folder_name,'/labels_',num2str(ij+init_num,'%07.f'),'.csv'];
            %filename_fields=[folder_name,'/fields_',num2str(ij+init_num,'%07.f'),'.csv'];
            %writematrix(Spec,filename_trace);
            %writematrix(labels,filename_labels);
            %writematrix(fields,filename_fields);
            
            if (~mod(ij,save_every))
                 f_name = [folder_name, '/traces_','axes.mat'];
                f_name = [folder_name, '/traces_', num2str(init_num+floor(ij_tot/save_every)),'.mat'];
               
                save(f_name, 'Spec_array', 'labels_array', 'E_IR_t_array', ...
                    'E_XUV_t_array', 'spec_XUV_abs_array', 'spec_XUV_phase_array')
                disp(['Traces dumped to file:', f_name])
                
                % Reset output variables
                Spec_array           = zeros([save_every, size(Spec)]);
                labels_array         = zeros([save_every, 11]); % Hard coded #labels saved
                E_IR_t_array         = zeros([save_every, length(t_IR_output)]);
                E_XUV_t_array        = zeros([save_every, length(t_XUV_output)]);
                spec_XUV_abs_array   = zeros([save_every, length(E_axis_XUV_output)]);
                spec_XUV_phase_array = zeros([save_every, length(E_axis_XUV_output)]);  
                
                ij = 0;
            end
            
            ij=ij+1;
            ij_tot = ij_tot+1;
        end
        
    end
    
end

% tilefigs_Jaco