% simulation FROG CRAB trace for ionization in a gas atom
% everything is in at. un.
% p and A are parallel. Formula from Mairesse

% no atomic phase included

%% constants
c = 299792458;
h = 6.62606957e-34;
el = 1.602176565e-19;
me = 9.1093826e-31;
eps0 = 8.854e-12;

%au
t_au = 2.418884326505e-17;
e_au = 4.35974417e-18;
m_au = 5.2917720859e-11;
int_au = 1.9446e-12;
c_au = c/m_au*t_au;
el_au = -1;

%% field definition

%IR
lambda = 800e-9;%1800e-9;%786e-9;
sigIR = 5e-15/t_au; % IR pulse time duration
Inten = 1e12; %in W/cm2 (was2)
omega_IR = 2*pi*c_au/(lambda/m_au); % IR frequency
omega_IR_si = 2*pi*c/(lambda); % IR frequency
A0 = sqrt(2*Inten*1e4*376.7303)*int_au;  %amplitude of the vector potential (w/cm^2)
CR = 0.0e30*t_au^2; %cirp rate 0.1
Env_ir = @(t) exp(-log(4)*t.^2/sigIR^2); %gaussian envelope

Eir = @(t) A0*Env_ir(t).*cos(omega_IR*t+CR/2*t.^2);

%% XUV definizione in tempo

Ip = 21.5646;%15.75;%15.75*el/e_au; %ionization potential of the gas in eV
%sigX = 300e-18/t_au; %Electron wp time duration
FWHM = 250e-18; %Electron wp time duration
sigX = FWHM/(2*sqrt(2*log(2)))/t_au;
hh_c = 27;%539;%363;%27;%54.6837;%56; %central energy of the attosecond pulse in HH (matches the CEP of the APT)
p1 = 30*t_au^2/(1e-30); %parabolic chirp in time 60 2600
A2 = 0;%0.6; %percentage of satellite in amplitude
p2 = -20*t_au/(1e-15); %additional linear chip on the satellite
Tp = lambda/c *1/t_au;

Env_xuv1 = @(t) exp(-t.^2/(2*sigX^2) + 1i*p1*t.^2); %gaussian envelope
Env_xuv2 = @(t) exp(-(t-Tp/2).^2/(2*sigX^2) + 1i*p2*(t-Tp/2).*exp(-2*(t-Tp/2).^2/(2*sigX^2))); %gaussian envelope

Exuv = @(t) (Env_xuv1(t)+A2*Env_xuv2(t)).*exp(1i*omega_IR*hh_c*t);
test = @(t) (Env_xuv1(t)+A2*Env_xuv2(t));

%% XUV definizione in frequenza

% Ip = 21.5646;%15.75;%15.75*el/e_au; %ionization potential of the gas in eV
% FWHM = 30e-18; %Electron wp time duration
% sigX = FWHM/(2*sqrt(2*log(2)));
% hh_c = 363;%27;%54.6837;%56; %central energy of the attosecond pulse in HH (matches the CEP of the APT)
% p2 = 0.1e-2*1e-30; %parabolic chirp in time 60
% omega = 2*pi*linspace(0,30e17,2^17); % axis for the initial definition
% df = (omega(2)-omega(1))/(2*pi);
% dt = 1/(length(omega)*df);
% t_xuv = [-length(omega)/2+[0:length(omega)-1]]*dt;
% 
% Exuv_f = exp(-0.5*(omega-omega_IR_si*hh_c).^2*sigX^2)...
%     .*exp(1i*p2*(omega-omega_IR_si*hh_c).^2); % spectrum in freq
% 
% Exuv_tf = fftshift(ifft(fftshift(Exuv_f)));
% Exuv_tf = Exuv_tf/max(Exuv_tf); % normalized pulse in time
% 
% if 1
%     figure
%     plot(t_xuv/1e-15,abs(Exuv_tf),'-o')
%     xlabel('Time (fs)','FontSize',16)
%     ylabel('Norm. Int','FontSize',16)
%     set(gcf,'Color',[1 1 1])
%     set(gca,'FontSize',14)
%     
%     figure
%     subplot(2,1,1)
%     plot(omega/1e17,abs(Exuv_f),'-o')
%     xlabel('2\pif (10^{17})','FontSize',16)
%     ylabel('Norm. Int','FontSize',16)
%     set(gcf,'Color',[1 1 1])
%     set(gca,'FontSize',14)
%     subplot(2,1,2)
%     plot(omega/1e17,unwrap(angle(Exuv_f)),'-ro')
%     xlabel('2\pif (10^{17})','FontSize',16)
%     ylabel('Phase (rad)','FontSize',16)
%     set(gcf,'Color',[1 1 1])
%     set(gca,'FontSize',14)
% end
%     
% 
% E_ab = fit(t_xuv.',abs(Exuv_tf).','cubicspline');
% E_ph = fit(t_xuv.',angle(Exuv_tf).','cubicspline');
% Exuv = @(t) E_ab(t*t_au).*exp(1i*E_ph(t*t_au));
% 
% test = @(t) (Exuv(t));

%% time and delay axis definition
%time
fn = 2^9;
fmax = 2.5*(hh_c*c/lambda - Ip*el/h)* 1e-15; %max frequency in fs-1
f = linspace(0,fmax,fn); %Frequency axis 
w = 2*pi*f; %omegas
W = f*1e15*h/el; %energies in eV

t = linspace(-30,30,2^13); %it was 25 %2^17
dt = t(2)-t(1);

%delays 
dtau = 0.1; %in fs %0.25
taun = 200; %number of dely points
tau = dtau*linspace(-(taun-1)/2,(taun-1)/2,taun);
taumin = min(tau);
taumax = max(tau);

%% plotting starting cond

test_s=test(t*1e-15/t_au);
test_s=test_s/max(abs(test_s));
phT=angle(test_s);
abT=abs(test_s);
phT(abT<0.01)=NaN;
phT=unwrap(phT);
phT=phT-min(phT);
figure
hold on
plot(t,abT,'-k')
set(gcf,'Color',[1 1 1])
set(gca,'FontSize',16)
xlabel('Time (fs)','FontSize',18)
ylabel('Norm. Int','FontSize',18)
box on

figure
hold on 
plot(t,unwrap(phT),'-k')
set(gcf,'Color',[1 1 1])
set(gca,'FontSize',16)
xlabel('Time (fs)','FontSize',18)
ylabel('Phase','FontSize',18)
box on

figure
plot(t,Eir(t*1e-15/t_au)/max(Eir(t*1e-15/t_au)),'-k')
hold on
set(gcf,'Color',[1 1 1])
set(gca,'FontSize',16)
xlabel('Time (fs)','FontSize',18)
ylabel('Norm. Ampl.','FontSize',18)
box on

%% evaluating the TL
test_f = fft(test_s);
test_f = abs(test_f);
test_s2=fftshift(ifft(test_f));
test_s2 = test_s2/max(test_s2);
idex=find(abs(test_s2)>=0.5);
t(idex(end))-t(idex(1)) % FWHM in fs TL
idex=find(abs(test_s)>=0.5);
t(idex(end))-t(idex(1)) % FWHM in fs Chirped


%% calculating trace with a for loop 
%direct path matrix product
Wfm = bsxfun(@times,W.'*el/e_au,ones(length(W),length(t)));
vfm = sqrt(2*Wfm);
tm = bsxfun(@times,t,ones(length(W),length(t)));

% pot and phase
A = - cumsum(Eir(tm*1e-15/t_au),2)*dt*1e-15/t_au;
A2 = 0.5*A.*A;
phi = -fliplr(cumsum(fliplr(vfm.*A+A2),2))*dt*1e-15/t_au;

%% direct path (only one foor loop)
a_tau_dir = zeros(fn,taun);
attendi = waitbar(0,'Looping dir... Please wait');
for ij = 1:taun
    integr = exp(-1i*phi).*Exuv((tm-tau(ij))*1e-15/t_au).*exp(-1i*(Wfm+Ip*el/e_au).*tm*1e-15/t_au);
    a_tau_dir(:,ij) = -1i*sum(integr,2).*dt*1e-15/t_au;
    waitbar(ij/taun,attendi);
end
close(attendi)
Spec = abs(a_tau_dir).^2;


%% double loop on IR
% % A = - cumsum(Eir(t*1e-15/t_au))*dt*1e-15/t_au;
% % A2 = 0.5*A.*A;
% 
% a_tau = zeros(fn,taun);
% attendi = waitbar(0,'Looping ... Please wait');
% for iv = 1:fn
%     Wf = W(iv)*el/e_au; %in a.u.
%     vf = sqrt(2*W(iv)*el/e_au); %final velocity in a.u.
%     for ij = 1:taun
%         A = - cumsum(Eir((t-tau(ij))*1e-15/t_au))*dt*1e-15/t_au;
%         A2 = 0.5*A.*A;
%         phi = -fliplr(cumsum(fliplr(vf*A+A2)))*dt*1e-15/t_au; %gate phase
%         integr = exp(-1i*phi).*Exuv((t)*1e-15/t_au).*exp(-1i*(Wf+Ip*el/e_au)*t*1e-15/t_au);
%         a_tau(iv,ij) = -1i*sum(integr).*dt*1e-15/t_au;
%     end
%     waitbar(iv/fn,attendi);
% end
% close(attendi)
% Spec = abs(a_tau).^2;

%% vector potential
% A = - cumsum(Eir(t*1e-15/t_au))*dt*1e-15/t_au;
% A2 = 0.5*A.*A;
% 
% %% calculating trace with a for loop
% 
% a_tau = zeros(fn,taun);
% attendi = waitbar(0,'Looping ... Please wait');
% for iv = 1:fn
%     Wf = W(iv)*el/e_au; %in a.u.
%     vf = sqrt(2*W(iv)*el/e_au); %final velocity in a.u.
%     for ij = 1:taun
%           phi = -fliplr(cumsum(fliplr(vf*A+A2)))*dt*1e-15/t_au; %gate phase
%         integr = exp(-1i*phi).*Exuv((t-tau(ij))*1e-15/t_au).*exp(-1i*(Wf+Ip*el/e_au)*t*1e-15/t_au);
%         a_tau(iv,ij) = -1i*sum(integr).*dt*1e-15/t_au;
%     end
%     waitbar(iv/fn,attendi);
% end
% close(attendi)
% Spec = abs(a_tau).^2;
% 
%% calculating trace with a for loop CM approx
% 
% a_tau = zeros(fn,taun);
% attendi = waitbar(0,'Looping ... Please wait');
% Wc = hh_c*c/lambda*h/e_au;
% vc = sqrt(2*Wc);
% phi = -fliplr(cumsum(fliplr(vc*A+A2)))*dt*1e-15/t_au; %gate phase
% for iv = 1:fn
%     Wf = W(iv)*el/e_au; %in a.u.
%     for ij = 1:taun
%         integr = exp(-1i*phi).*Exuv((t-tau(ij))*1e-15/t_au).*exp(-1i*(Wf+Ip*el/e_au)*t*1e-15/t_au);
%         a_tau(iv,ij) = -1i*sum(integr).*dt*1e-15/t_au;
%     end
%     waitbar(iv/fn,attendi);
% end
% close(attendi)
% Spec = abs(a_tau).^2;

%% plotting

figure
%pcolor(tau,W+Ip,Spec)
pcolor(tau,W,Spec)
ylabel('Photon energy (eV)','FontSize',20)
xlabel('Delay (fs) (XUV delayed)','FontSize',20)
set(gcf,'Color',[1 1 1])
set(gca,'FontSize',18)
shading flat

