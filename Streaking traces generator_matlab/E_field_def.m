%% FoS, ETH Zurich, 27 Dec 2019

%% constants
c = 299792458;
h = 6.62606957e-34;
el = 1.602176565e-19;
me = 9.1093826e-31;
eps0 = 8.854e-12;

%au
t_au = 2.418884326505e-17;
e_au = 4.35974417e-18;
m_au = 5.2917720859e-11;
int_au = 1.9446e-12;
c_au = c/m_au*t_au;
el_au = -1;

%% field definition

N_t=300;
t = linspace(-30,30,N_t);

%IR
lambda = 785e-9;
sigIR = 10e-15/t_au; % IR pulse time duration
Inten = 2e12; %in W/cm2
CR = 0e-4;

omega_IR = 2*pi*c_au/(lambda/m_au); % IR frequency
omega_IR_si = 2*pi*c/(lambda); % IR frequency
A0 = sqrt(2*Inten*1e4*376.7303)*int_au;  %amplitude of the vector potential (w/cm^2)
%CR = 0.1e30*t_au^2; %cirp rate 0.1
Env_ir = @(t) exp(-log(4)*t.^2/sigIR^2); %gaussian envelope

Eir = @(t) A0*Env_ir(t).*cos(omega_IR*t+CR/2*t.^2);
Eir = @(t) A0*Env_ir(t).*cos(CR/2*t.^2);

E_ir = Eir(t*1e-15/t_au)/max(Eir(t*1e-15/t_au));

%%
figure(301); clf;
plot(t,E_ir,'Color','r')


%% Prepare time axis
t_fs=t*1e-15;
dt=t_fs(2)-t_fs(1);
L=length(t_fs);

Fs=1/dt;
f = Fs*(0:(L/2))/L;
l_nm = 299792458./f*1e9;

%% Transform IR field

E_ir_fft = fft(E_ir);
E_ir_fft_singleside=E_ir_fft(1:L/2+1);
E_ir_fft_singleside(2:end-1)=2*E_ir_fft_singleside(2:end-1);


%% Plot IR field
figure(302); clf;
% subplot(1,2,1)
plot(f, abs(E_ir_fft_singleside).^2, '-o')
%plot(l_nm, abs(E_ir_fft_singleside).^2, '-o')

yyaxis right
plot(f, unwrap(angle(E_ir_fft_singleside)),'-o')
%plot(l_nm, unwrap(angle(E_ir_fft_singleside)),'-o')

legend('power', 'angle')

% xlim([f_cent-200e12, f_cent+200e12])
%xlim([500, 1100])

% subplot(1,2,2)
% plot(f, unwrap(angle(E_ir_fft_singleside)),'-o')
% xlim([f_cent-200e12, f_cent+200e12])

