function [tau,W,Spec,t,E_IR_output,E_XUV_output,tau_FWHM,T_IR]=function_Streaking_strong_field_Jaco(sigIR,Inten,CR,xuv_FWHM,xuv_c,p2,p3,p4,p5,E_max,E_n,dtau,taun)
%% makes RABBITT with different wavelenghts

% simulation FROG CRAB trace for ionization in a gas atom
% everything is in at. un.
% p and A are parallel. Formula from Mairesse

%% constants
c = 299792458;
h = 6.62606957e-34;
el = 1.602176565e-19;
me = 9.1093826e-31;
eps0 = 8.854e-12;

%au
t_au = 2.418884326505e-17;
e_au = 4.35974417e-18;
m_au = 5.2917720859e-11;
int_au = 1.9446e-12;
c_au = c/m_au*t_au;
el_au = -1;


%% Parameters which can be varied
%IR parameters
sigIR = sigIR*1e-15/t_au;

CR = CR*1e30*t_au^2; 

p2 = p2*1e-30; 


%% field definition

%IR
lambda = 785e-9;
%sigIR = 6e-15/t_au; % IR pulse time duration
%Inten = 2e12; %in W/cm2
omega_IR = 2*pi*c_au/(lambda/m_au); % IR frequency
omega_IR_si = 2*pi*c/(lambda); % IR frequency
A0 = sqrt(2*Inten*1e4*376.7303)*int_au;  %amplitude of the vector potential (w/cm^2)
%CR = 0.1e30*t_au^2; %cirp rate 0.1
Env_ir = @(t) exp(-log(4)*t.^2/sigIR^2); %gaussian envelope

Eir = @(t) A0*Env_ir(t).*cos(omega_IR*t+CR/2*t.^2);

%% xuv pulse train
%p2 = -2e-2*1e-30;
Ip = 15.7596;%15.75*el/e_au; %ionization potential of the gas in eV
%sigma_SAP = 410e-18; %Electron wp time duration
%hh_c = 20.5; %central HH of the attosecond pulse in HH (matches the CEP of the APT)
%sigma_APT = 0.;1.e-15; %APT time duration
%hh_c = 22.5
hh_c = xuv_c/omega_IR_si/h*2*pi*el;
kind='sin';
%axis
Nt_xuv = 2^18;
t_xuv = linspace(-80,80,Nt_xuv)*1e-15;
dt = t_xuv(2)-t_xuv(1);
df = 1/(Nt_xuv*dt);
f_xuv = [-Nt_xuv/2+[0:Nt_xuv-1]]*df;
e_xuv = h*f_xuv/abs(el);
%[ene_mod, hnu, A_f0, Exuv_fi, Exuv_ti] = APT_xuv(t_xuv,f_xuv,lambda,sigma_SAP,hh_c,sigma_APT,kind,0);
[ene_mod, hnu, A_f0, Exuv_fi, Exuv_ti] = APT_xuv_Jaco(t_xuv,f_xuv,lambda,xuv_FWHM,hh_c,kind,0);

%% introducing chirp
Exuv_fi = Exuv_fi.*exp(1i*(p2*(f_xuv*2*pi-omega_IR_si*hh_c).^2 ...
    +p3*((f_xuv*2*pi-omega_IR_si*hh_c)/1e15).^3 ...
    +p4*((f_xuv*2*pi-omega_IR_si*hh_c)/1e15).^4 ...
    +p5*((f_xuv*2*pi-omega_IR_si*hh_c)/1e15).^5));

%% adding cross section
% % %%%%%%%%% West and Marr %%%%%%%%%%
cs_temp=load('Ar_WestMarr.txt','-ascii');
cs_ene = h*c./(cs_temp(:,1)*1e-10)*1/abs(el);
cs_amp=cs_temp(:,2);
cs=interp1(cs_ene,cs_amp,e_xuv);
cs(isnan(cs))=0;

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%cs = ones(size(e_xuv)); %uncomment to remove the cs%
Exuv_fi = Exuv_fi.*cs;

%% adding the atomic phase
if (0)
    %%%  Mauritsson  %%%%%%%%%%%%%%
    load('Ar_phase_Mau.mat');
    fun_at = fit(eneL.',phase,'cubicspline');
    ind_l =  find(e_xuv < 15.77); % making the phase constant at the borders %25.4 %24.7
    ind_l2 = find(eneL < 15.77,1,'Last');
    ind_r =  find(e_xuv > 96.594); %41.8 %42.8
    ind_r2 = find(eneL > 96.594,1,'First');

    phase_ext = fun_at(e_xuv);
    phase_ext(ind_l) = phase(ind_l2);
    phase_ext(ind_r) = phase(ind_r2);

    %Exuv_ff = Exuv_fi.*exp(1i.*phase_ext'); % with atomic phase
else
    Exuv_ff = Exuv_fi; % no atomic phase    
end


%% back transforming

Exuv_tf = fftshift(ifft(fftshift(Exuv_ff)));
Exuv_tf = Exuv_tf/max(Exuv_tf);


%% XUV in time
E_ab = fit(t_xuv.',abs(Exuv_tf).','cubicspline');
%E_ph = fit(t_xuv.',angle(Exuv_tf).','cubicspline');
E_ph = fit(t_xuv.',unwrap(angle(Exuv_tf)).','cubicspline');
Exuv = @(t) E_ab(t*t_au).*exp(1i*E_ph(t*t_au));
Exuv_env = @(t) E_ab(t*t_au);

%% time and delay axis definition
%time
%fn = 2^6;
fn = E_n;
%fmax = 2*(hh_c*c/lambda - 18*el/h)* 1e-15; %max frequency in fs-1
fmax = E_max*el/h* 1e-15; %max frequency in fs-1
f = linspace(0,fmax,fn); %Frequency axis
w = 2*pi*f; %omegas
W = f*1e15*h/el; %energies in eV

N_t=2^12;
t = linspace(-30,30,N_t);
dt = t(2)-t(1);

%delays
 %in fs
step_ratio=ceil(dtau/dt);

tau = step_ratio*dt*linspace(-(taun-1)/2,(taun-1)/2,taun);
taumin = min(tau);
taumax = max(tau);

envelope=Exuv_env(t*1e-15/t_au)/max(real(Exuv_env(t*1e-15/t_au)));
gauss_fit = fit(t',envelope,'gauss1');
tau_FWHM=2*sqrt(log(2))*gauss_fit.c1;

T_IR=2*pi/omega_IR_si*10^15;

%% plotting starting cond (in TIME)

if(0)
    test_s=Exuv(t*1e-15/t_au); % Complex field (envelope + oscillating part)
    test_s=test_s/max(abs(test_s));

    %save('temp.mat','t', 'test_s')

    phT=angle(test_s);
    abT=abs(test_s);
    low_int = abT<0.01;
    abT(low_int)=NaN; % Such that itensity and phase are plotted with the same xlim
    phT(low_int)=NaN;
    phT=unwrap(phT);
    phT=phT-min(phT);

    nfig=100;
    nfig=nfig+1;
    figure(nfig); clf;
    subplot(3,1,1)
    hold on
    plot(t,abT,'-k')
    set(gcf,'Color',[1 1 1])
    set(gca,'FontSize',16)
    xlabel('Time (fs)','FontSize',18)
    ylabel('Norm. Int','FontSize',18)
    box on

    subplot(3,1,2)
    hold on
    plot(t,unwrap(phT),'-k')
    set(gcf,'Color',[1 1 1])
    set(gca,'FontSize',16)
    xlabel('Time (fs)','FontSize',18)
    ylabel('Phase','FontSize',18)
    box on
    title('Starting conditions in time')

    subplot(3,1,3) 
    hold on
    plot(t,Eir(t*1e-15/t_au)/max(Eir(t*1e-15/t_au)),'Color','r')
    plot(t,Exuv(t*1e-15/t_au)/max(real(Exuv(t*1e-15/t_au))),'Color','b')
    plot(t,Exuv_env(t*1e-15/t_au)/max(real(Exuv_env(t*1e-15/t_au))),'Color','b')
    hold on
    set(gcf,'Color',[1 1 1])
    set(gca,'FontSize',16)
    xlabel('Time (fs)','FontSize',18)
    ylabel('Norm. Ampl.','FontSize',18)
    box on
end

%% plotting starting cond (in FREQ)

if(0)

    L=length(test_s);
    Fs=1/(dt*1e-15);
    f = Fs*(0:(L/2))/L;
    f_ev = f./2.417989242e14;

    test_s = fftshift(test_s); % fftshift the XUV to avoid time shift
    
    % Plot the XUV field
    %figure(603)
    %plot(1:length(test_s), test_s)
    
    E_XUV_fft = fft(test_s);
    E_XUV_fft = E_XUV_fft(1:L/2+1);
    
    ab_E_XUV_FFT = abs(E_XUV_fft);
    ph_E_XUV_FFT = angle(E_XUV_fft);
    %save('temp.mat', 'ph_E_XUV_FFT')
    low_int = ab_E_XUV_FFT<1;
    ab_E_XUV_FFT(low_int)=NaN;
    ph_E_XUV_FFT(low_int)=NaN;

    %E_XUV_fft_singleside(2:end-1)=2*E_XUV_fft_singleside(2:end-1);

    % Plot XUV field
    figure(602); clf;
    plot(f_ev, ab_E_XUV_FFT.^2, '-o')
    xlabel('Energy [eV]')
    ylabel('Intensity')
    title('Initial conditions in freq')

    yyaxis right
    %plot(f_ev, unwrap(ph_E_XUV_FFT)-pi*linspace(0,length(ph_E_XUV_FFT),length(ph_E_XUV_FFT))','-o')
    plot(f_ev, unwrap(ph_E_XUV_FFT),'-o')
    ylabel('Phase')

end
    
%% vector potential

E_IR_output=Eir(t*1e-15/t_au);
E_XUV_output=real(Exuv(t*1e-15/t_au));
%E_XUV_output=Exuv(t*1e-15/t_au);

A = - cumsum(Eir(t*1e-15/t_au))*dt*1e-15/t_au;
A2 = 0.5*A.*A;

%% calculating trace with a for loop
t_xuv_full=(t(1)+tau(1)):dt:(t(end)+tau(end));
Exuv_full=Exuv(t_xuv_full*1e-15/t_au);

a_tau = zeros(fn,taun);

%attendi = waitbar(0,'Looping ... Please wait');
%tic;
ind_matrix=ones(N_t,taun);
ind_matrix=ind_matrix.*(1:N_t)';
ind_matrix=ind_matrix+(0:taun-1)*step_ratio;

for iv = 1:fn
    
    Wf = W(iv)*el/e_au; %in a.u.
    vf = sqrt(2*W(iv)*el/e_au); %final velocity in a.u.
    phi = -fliplr(cumsum(fliplr(vf*A+A2)))*dt*1e-15/t_au; %gate phase
    exp_phi_Wf=exp(-1i*phi).*exp(-1i*(Wf+Ip*el/e_au)*t*1e-15/t_au);
    
    for ij = 1:taun

        integrand=exp_phi_Wf.*Exuv_full(1+step_ratio*(ij-1):N_t+step_ratio*(ij-1)).';
        
        a_tau(iv,ij) = -1i*sum(integrand);

    end
    
    %waitbar(iv/fn,attendi);
    
end
%toc;
%close(attendi)
%%%%%


Spec = abs(a_tau.*dt*1e-15/t_au).^2;


%% plotting

if(0)
    % Streaking trace colormap
    nfig=nfig+1;
    figure(nfig)
    pcolor(tau,W,Spec)
    ylabel('Electron energy (eV)','FontSize',20)
    xlabel('Delay (fs) (XUV delayed)','FontSize',20)
    set(gcf,'Color',[1 1 1])
    set(gca,'FontSize',18)
    shading interp
    colorbar
end
% 
% tilefigs_Jaco

end

