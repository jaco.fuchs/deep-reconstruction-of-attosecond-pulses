function [ene_mod, hnu, A_f0, F_f, Exuv_t]=APT_xuv_Jaco(t,f,lambda,xuv_FWHM,hh_c,kind,verb)
% t = time axis in SI
% f = frequency axis to which t transfroms in SI 
% sigma_SAP time duration single pulse
% sigma_APT time duration envelope APT
% hh_c 
% kind must be 'cos' or 'sin' for different APTs
% verb = 1 if you want the plots

%costants
el=-1.60217653e-19;
c=299792458;
h=6.626e-34;
%central freq
omega_l=c/lambda*2*pi;
%central ene
hnu=h*omega_l/(2*pi*abs(el));
%A_f
omega_c=2*pi*hh_c*c/lambda; %seleziona ordine dispari
ene_mod=f/abs(el)*h; %energy axis

%c1=sigma_SAP/(2*sqrt(2*log(2)));
%A_f0=0.5*sqrt(2*pi)*c1*(exp(-2*pi^2*c1^2*(f-omega_c/(2*pi)).^2)+exp(-2*pi^2*c1^2*(f+omega_c/(2*pi)).^2));
% c1=sigma_SAP/(4*sqrt(log(2)));
% A_f0=sqrt(pi)*c1*(exp(-c1^2*(2*pi*f-omega_c).^2)+exp(-c1^2*(2*pi*f+omega_c).^2));

c1=2*pi*xuv_FWHM/h*el/2*sqrt(log(2));
c1=c1*sqrt(2);%
A_f0=sqrt(pi)*1/c1*(exp(-1/c1^2*(2*pi*f-omega_c).^2)+exp(-1/c1^2*(2*pi*f+omega_c).^2));

%train P_f
tau=lambda/(c*2);
P_f=A_f0;
for ij=40:40
    P_f=P_f+A_f0.*exp(-1i*ij*(2*pi*f-omega_l)*tau);
    P_f=P_f+A_f0.*exp(+1i*ij*(2*pi*f-omega_l)*tau);
end
if strcmp(kind,'cos')
   P_f=P_f.*exp(1i*2*pi*f*tau/2);
end
%G_f
% c2=sigma_APT/(2*sqrt(2*log(2)));
% G_f=sqrt(2*pi)*c2*exp(-2*pi^2*c2^2*f.^2);
% %F_f
% F_f=conv((P_f),G_f,'same');
F_f=P_f;
%going back to time domain
Exuv_t=ifftshift(ifft(ifftshift(F_f)));
if verb
    figure
    plot(ene_mod,abs(A_f0).^2/max(abs(A_f0).^2),'k')
    hold on
    plot(ene_mod,abs(F_f).^2/max(abs(F_f).^2),'r')
    set(gca,'FontSize',14)
    set(gcf,'Color',[1 1 1])
    xlabel('Energy (eV)','FontSize',16)
    ylabel('Norm. spectral int.','FontSize',16)
    xlim([16 50])
    
    figure
    plot(t/1e-15,real(Exuv_t)/max(real(Exuv_t)),'-')
    set(gca,'FontSize',14)
    set(gcf,'Color',[1 1 1])
    xlabel('Time (fs)','FontSize',16)
    ylabel('Norm. amplitude','FontSize',16)
end
